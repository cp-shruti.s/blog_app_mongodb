<?php
require 'vendor/autoload.php';
try {

// connect to mongodb
    $client = new MongoDB\Client("mongodb://localhost:27017");
    $db = $client->blog_posts;
    $collection = $db->posts;

    $postId = $_POST['post'];
    $userId = $_POST['user'];

    $objectId = new \MongoDB\BSON\ObjectId($postId);

    $result = ['userId' => $userId, 'postId' => $postId];
    echo json_encode($result);
//$collection->updateOne(['_id' => '567eba6ea0b67b21dc004687'], ['$set' => ['some_property' => 'some_value']]);
    $cursor = $collection->updateOne(['_id' => $objectId], ['$set' => ['is_like' => 1]]);
    echo "update";
    printf("Matched %d document(s)\n", $cursor->getMatchedCount());
    printf("Modified %d document(s)\n", $cursor->getModifiedCount());

}catch (\MongoDB\Driver\Exception\Exception $e){
    die($e);
}