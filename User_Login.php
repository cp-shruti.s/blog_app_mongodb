
<?php
require_once __DIR__ . '/vendor/autoload.php';
session_start();
try{
    $client = new MongoDB\Client("mongodb://localhost:27017");

    $db =$client->blog_posts;
    $collection =$db->user;

    $username = $_POST['username'];
    $password = $_POST['password'];

    if (!empty($username) && !empty($password)){

    $insert = array("username" => $username, "password" => $password);
    $result = $collection->findOne($insert);
    if (!empty($result)){
        $_SESSION['_id'] = $result['_id'];
        $_SESSION['username'] = $result['username'];
        echo "Login successfully";
    }else{
        echo "wrong username and password";
    }

    if (isset($_SESSION["_id"])){
        header("location:User_HomePage.php");
        echo $_SESSION["_id"];
    }
    }

}catch(\MongoDB\Driver\Exception\Exception $e){
    die($e);
}
?>


<!DOCTYPE html>
<html lang="em">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="center">
    <form action="User_Login.php" method="post">
        <div class="container">
            <h1>Login</h1>
            <p>Please fill in this form and Login in this apps</p>
            <hr>

            <label for="email"><b>Username</b></label>
            <input type="text" placeholder="Enter Username" name="username" id="email" required>

            <label for="psw"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="password" id="psw" required>

            <a href="User_Registration.php">Register here!!</a>
            <hr>

            <button type="submit" class="registerbtn" name="submit">Login</button>
        </div>


    </form>
</div>
</body>
</html>




