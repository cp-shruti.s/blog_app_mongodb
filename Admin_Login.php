<?php
require_once __DIR__ . '/vendor/autoload.php';

try{
    $client = new MongoDB\Client("mongodb://localhost:27017");

    $db =$client->blog_posts;
    $collection =$db->admin;

    $username = $_POST['username'];
    $password = $_POST['password'];

    $insert = array("username" => $username, "password" => $password);
    $result = $collection->findOne($insert);

    if (!empty($username) && !empty($password)) {
        if (!empty($result)) {
            echo "Login successfully";
            header("location:display_Posts.php");
        } else {
            echo "wrong username and password";
        }
    }
//    $user =$db->$collection->findOne(array('username'=>'user1','password'=>'pass1' ));
//
//    foreach ($user as $obj){
//        echo 'username'.$obj['username'];
//        echo 'password'.$obj['password'];
//        if ($username == 'user1' && $password == 'pass1'){
//            echo 'found';
//            header("location:display_Posts.php");
//        }else{
//            echo 'not found';
//        }
//    }

}catch(\MongoDB\Driver\Exception\Exception $e){
    die($e);
}
?>

<!DOCTYPE html>
<html lang="em">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="center">
    <form action="Admin_Login.php" method="post">
        <div class="container">
            <h1>Login</h1>
            <p>Please fill in this form and Login in this apps</p>
            <hr>

            <label for="email"><b>Username</b></label>
            <input type="text" placeholder="Enter Username" name="username" id="email" required>

            <label for="psw"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="password" id="psw" required>

            <a href="admin_registration.php">Register here!!</a>
            <hr>

            <button type="submit" class="registerbtn" name="submit">Login</button>
        </div>


    </form>
</div>
</body>
</html>



