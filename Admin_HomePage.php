<?php
require_once __DIR__ . "/vendor/autoload.php";

try{
    if(isset($_POST['submit'])) {
        $client = new MongoDB\Client("mongodb://localhost:27017");

        $db = $client->blog_posts;
        $collection = $db->posts;

        $data = [
            'title' => $_POST['title'],
            'description' => $_POST['description'],
            'date' => $_POST['date'],
            'author' => $_POST['author'],
            'category' => $_POST['category'],
            'image' => $_FILES['file']['name'],
            'is_like' => 0
        ];
        if (!empty($data)) {

            if ($_FILES['file']) {
                if (move_uploaded_file($_FILES['file']['tmp_name'], 'image/' . $_FILES['file']['name'])) {
                    echo "<script>alert('uploaded file')</script>";
                } else {
                    echo "<script>alert('not file uploaded')</script>";
                }
            }
            $result = $collection->insertOne($data);
            if ($result->getInsertedCount() > 0) {
                echo "<script>alert('created articles')</script>";
                header("location:display_Posts.php");
            } else {
                echo "<script>alert('failed articles')</script>";
            }
        }
        else{
            echo "data is empty";
        }
    }
}catch(\MongoDB\Driver\Exception\Exception $e){
    die($e);
}
?>

<!DOCTYPE html>
<html lang="em">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="center">
    <form action="Admin_HomePage.php" method="post" enctype="multipart/form-data">
        <div class="container">
            <h1>Register</h1>
            <p>Please fill in this form </p>
            <hr>

            <label for="title"><b>Posts Title</b></label>
            <input type="text" placeholder="Enter Posts Title" name="title" required>

            <label for="description"><b>Posts Description</b></label>
            <input type="text" placeholder="Enter Posts Description" name="description" required>

            <label for="date"><b>Posts Created Date</b></label>
            <input type="date" placeholder="Enter Published date" name="date" required>

            <label for="author"><b>Author Name</b></label>
            <input type="text" placeholder="Enter Author name" name="author" required>

            <label for="category"><b>Posts Category</b></label>
            <input type="text" placeholder="Enter Category name" name="category" required>

            <label for="file1"><b>Image</b></label>
            <input type="file"  name="file" accept="Image/" required>

            <hr>
            <button type="submit" class="registerbtn" name="submit" >Submit</button>
        </div>
    </form>
</div>
</body>
</html>


