<?php
require_once __DIR__ . '/vendor/autoload.php';

try{
    $client = new MongoDB\Client("mongodb://localhost:27017");

    $db =$client->blog_posts;
    $collection =$db->user;

//    $document = array(
//        "firstname" => "bhumi",
//        "lastname" => "patel",
//        "username" =>"bhumipatel",
//        "password" =>"bhumi"
//    );
//
//    $collection->insertOne($document);
//    echo"document inserted";
    if (isset($_POST['submit'])) {
        $insertOneResult = [
            'firstname' => $_POST['firstname'],
            'lastname' => $_POST['lastname'],
            'username' => $_POST['username'],
            'password' => $_POST['password'],
        ];
        if (!empty($insertOneResult)) {
            $result = $collection->insertOne($insertOneResult);
            if ($result->getInsertedCount() > 0) {
                echo "user registration success";
                header("location:User_HomePage.php");
            } else {
                echo "<script>alert('failed articles')</script>";
            }
        } else {
            echo "data not insert";
        }
    }
}catch(\MongoDB\Driver\Exception\Exception $e){
    die($e);
}
?>

<!DOCTYPE html>
<html lang="em">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="center">
    <form action="User_Registration.php" method="post">
        <div class="container">
            <h1>Register</h1>
            <p>Please fill in this form </p>
            <hr>

            <label for="email"><b>First Name</b></label>
            <input type="text" placeholder="Enter Firstname" name="firstname" id="email" required>

            <label for="email"><b>Last Name</b></label>
            <input type="text" placeholder="Enter Lastname" name="lastname" id="email" required>

            <label for="email"><b>Username</b></label>
            <input type="text" placeholder="Enter Username" name="username" id="email" required>

            <label for="psw"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="password" id="psw" required>


            <a href="User_Login.php">Login here!!</a>
            <hr>

            <button type="submit" class="registerbtn" name="submit">Submit</button>
        </div>
    </form>
</div>
</body>
</html>